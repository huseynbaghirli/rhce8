<?php
$user = "php_user";
$password = "123";
$database = "ingress";
$table = "students";

try {
  $db = new PDO("mysql:host=localhost;dbname=$database", $user, $password);
  echo "<h2 style='color: #009688;'>Bizim Kruq</h2>";
  echo "<table class='styled-table'>";
  echo "<thead><tr><th style='background-color: #009688;'>ID</th><th style='background-color: #009688;'>Ad</th><th style='background-color: #009688;'>Soyad</th><th style='background-color: #009688;'>Yaş</th></tr></thead>";
  echo "<tbody>";
  foreach($db->query("select * from $table") as $row) {
    echo "<tr><td>" . $row['ID'] . "</td><td>" . $row['NAME'] . "</td><td>" . $row['SURNAME'] . "</td><td>" . $row['AGE'] . "</td></tr>";
  }
  echo "</tbody>";
  echo "</table>";
} catch (PDOException $e) {
  print "Bağlantı hatası: " . $e->getMessage() . "<br/>";
  die();
}
?>

<style>
/* style.css */
.styled-table {
  width: 100%;
  border-collapse: collapse;
  margin: 25px 0;
  font-size: 1em;
  font-family: Arial, sans-serif;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}

.styled-table thead th {
  padding: 12px 15px;
  font-weight: bold;
  color: #fff;
}

.styled-table tbody td {
  padding: 12px 15px;
}

.styled-table tbody tr:nth-of-type(odd) {
  background-color: #f9f9f9;
}

.styled-table tbody tr:hover {
  background-color: #f2f2f2;
}
</style>
